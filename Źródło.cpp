#include <iostream>
using namespace std;

class LinkedList
{
public:
	int value;
	LinkedList* next = NULL;
	LinkedList* prev = NULL;
	LinkedList* additionalPointer = NULL;

	LinkedList(int val = 0)
	{
		value = val;
	}
};

class struktura128
{
public:

	int n;
	bool* array;
	LinkedList start;

	struktura128(int number)
	{
		n = number;
		array = new bool[number];
		for (int i = 0; i < n; i++)
		{
			array[i] = false;
		}
	}

	void select()
	{
		if (start.next != NULL)
		{
			LinkedList* whereToDelete = start.next;
			start.next = start.next->next;
			array[whereToDelete->value - 1] = false;
			delete whereToDelete;
		}
	}

	void insert(int number)
	{
		if (array[number - 1] == false)
		{
			LinkedList* toInsert = new LinkedList(number);
			(*toInsert).next = start.next;
			start.next = toInsert;
			array[number - 1] = true;
		}

	}

	void search(int number)
	{
		if (array[number - 1] == true)
		{
			cout << "Element istnieje w zbiorze" << endl;
		}
		else
		{
			cout << "Element nie istnieje w zbiorze" << endl;
		}
	}

	~struktura128()
	{
		delete array;
		while (start.next != NULL)
		{
			LinkedList* whereToDelete = start.next;
			start.next = start.next->next;
			array[whereToDelete->value] = false;
			delete whereToDelete;
		}
	}

};

class struktura129
{
public:

	int n;
	LinkedList start;
	LinkedList* array;

	struktura129(int number)
	{
		n = number;
		array = new LinkedList[number];
	}

	void push(int number)
	{
		LinkedList* toInsert = new LinkedList(number);
		(*toInsert).next = start.next;
		start.next = toInsert;
		if (toInsert->next != NULL)
		{
			toInsert->next->prev = toInsert;
		}

		LinkedList* add = new LinkedList;
		add->next = (&array[number - 1])->next;
		(&array[number - 1])->next = add;
		add->additionalPointer = toInsert;
	}

	void pop()
	{
		if (start.next != NULL)
		{
			LinkedList* toDelete = start.next;
			LinkedList* toDeleteFromArray = (&array[toDelete->value - 1])->next;

			start.next = start.next->next;
			(&array[toDelete->value - 1])->next = (&array[toDelete->value - 1])->next->next;

			delete toDelete;
			delete toDeleteFromArray;
		}

	}

	void search(int number)
	{
		if (array[number - 1].next != NULL)
		{
			cout << "Element istnieje w ciagu" << endl;
		}
		else
		{
			cout << "Element nie istnieje w ciagu" << endl;
		}
	}

	void Delete(int number)
	{
		if (array[number - 1].next != NULL)
		{
			LinkedList* toDelete = array[number - 1].next;
			array[number - 1].next = array[number - 1].next->next;

			LinkedList* toDeleteTwo = toDelete->additionalPointer;
			toDeleteTwo->prev->next = toDeleteTwo->next;
			if (toDeleteTwo->next != NULL)
			{
				toDeleteTwo->next->prev = toDeleteTwo->prev;
			}

			delete toDeleteTwo;
			delete toDelete;
		}
	}

	void wypisz() //testowa�em sobie u�ywaj�c tego dzia�anie programu
	{
		LinkedList* k = start.next;

		while (k != NULL)
		{
			cout << k->value << " ";
			k = k->next;
		}
		cout << endl;
	}

	~struktura129()
	{
		for (int number = 0; number < n; number++)
		{
			while (array[number].next != NULL)
			{
				LinkedList* toDelete = array[number].next;
				array[number].next = array[number].next->next;
				delete toDelete->additionalPointer;
				delete toDelete;
			}
		}

		delete array;
	}

};

class struktura130
{
public:

	LinkedList start;
	LinkedList trashList;
	LinkedList smallestList;
	LinkedList* lastElement;
	LinkedList* lastElementInTrash;
	LinkedList* lastSmallestElement;

	struktura130()
	{
		lastElement = &start;
		lastElementInTrash = &trashList;
		lastSmallestElement = &smallestList;

		start.value = INT_MAX;
		smallestList.value = INT_MAX;
	}

	void push(int number)
	{
		LinkedList* toInsert = new LinkedList(number);
		lastElement->next = toInsert;
		toInsert->prev = lastElement;
		lastElement = toInsert;

		if (toInsert->value <= lastSmallestElement->value)
		{
			lastSmallestElement->next = new LinkedList(toInsert->value);
			LinkedList* additional = lastSmallestElement->next;
			lastSmallestElement->next->prev = lastSmallestElement;
			lastSmallestElement->next->additionalPointer = toInsert;
			lastSmallestElement = lastSmallestElement->next;
		}
	}

	void pop()
	{
		if (&start != lastElement && &smallestList != lastSmallestElement)
		{
			if (lastSmallestElement->additionalPointer = lastElement)
			{
				lastSmallestElement = lastSmallestElement->prev;
				delete (lastSmallestElement->next);
				lastSmallestElement->next = NULL;
			}

			lastElement = lastElement->prev;
			delete (lastElement->next);
			lastElement->next = NULL;
		}
	}

	void uptomin()
	{
		if (&start != lastElement && &smallestList != lastSmallestElement)
		{
			LinkedList* toDelete = lastSmallestElement;
			LinkedList* toTrashList = lastSmallestElement->additionalPointer;

			lastSmallestElement = lastSmallestElement->prev;
			lastSmallestElement->next = NULL;
			delete toDelete;

			lastElementInTrash = lastElement;
			lastElement = toTrashList->prev;
			lastElement->next = NULL;
		}
	}

	void wypisz()
	{
		LinkedList* current = start.next;

		while (current != NULL)
		{
			cout << current->value << " ";
			current = current->next;
		}
		cout << endl;
	}

	~struktura130()
	{
		lastElementInTrash->next = start.next;

		LinkedList* current = trashList.next;
		LinkedList* toDelete;

		while (current != NULL)
		{
			toDelete = current;
			current = current->next;
			delete toDelete;
		}
	}
};